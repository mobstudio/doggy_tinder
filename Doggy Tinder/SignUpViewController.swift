//
//  SignUpViewController.swift
//  ParseStarterProject
//
//  Created by Felipe Mota on 04/08/2015.
//  Copyright © 2015 Parse. All rights reserved.
//

import UIKit
import Parse

class SignUpViewController: UIViewController {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var interestedInWomen: UISwitch!
    @IBOutlet weak var signUp: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //mockUsers()
        
        let graphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields" : "id, name, gender, email"])
        graphRequest.startWithCompletionHandler( {
            (connection, result, error) -> Void in
            if error != nil {
                print(error)
            } else if let result = result {
                
                print(result)
                PFUser.currentUser()?["gender"] = result["gender"]
                PFUser.currentUser()?["name"] = result["name"]
                PFUser.currentUser()?["email"] = result["email"]
                PFUser.currentUser()?["interestedInWomen"] = self.interestedInWomen.on
                PFUser.currentUser()?.save()
                
                let userId = result["id"] as! String
                
                let facebookProfilePictureUrl = "https://graph.facebook.com/" + userId + "/picture?type=large"
                
                if let fbpicUrl = NSURL(string: facebookProfilePictureUrl) {
                    
                    if let data = NSData(contentsOfURL: fbpicUrl) {
                        
                        self.userImage.image = UIImage(data: data)
                        let imageFile:PFFile = PFFile(data: data)
                        
                        PFUser.currentUser()?["image"] = imageFile
                        PFUser.currentUser()?.save()
                    }
                }
            }
        })
    }
    
    func mockUsers() {
        let urlArray = [
            "http://cdn.cnwimg.com/thumb-300x300/fBaseOrig/m/05v09jj.jpg",
            "http://www.polyvore.com/cgi/img-thing?.out=jpg&size=l&tid=37697433",
            "http://acidezmental.xpg.uol.com.br/imagens/oscar/marilynmonroe.jpg",
            "http://www.linkirado.net/img_post/link-irado-97e5802e68bcfb10c359065594d2105b.jpg",
            "https://princesarefinada.files.wordpress.com/2015/04/38151579.jpg"
        ]
        
        var counter = 1
        for url in urlArray {
            
            let nsUrl = NSURL(string: url)

            if let data = NSData(contentsOfURL: nsUrl!) {
                self.userImage.image = UIImage(data: data)
                
                let imageFile:PFFile = PFFile(data: data)
                
                let user:PFUser = PFUser()

                let userName = "user\(counter)"
                user.username = userName
                user.password = "pass"
                user["image"] = imageFile
                user["interestedInWomen"] = false
                user["gender"] = "female"
                
                counter++
                user.signUp()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
