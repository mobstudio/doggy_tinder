//
//  SwipingViewController.swift
//  ParseStarterProject
//
//  Created by Felipe Mota on 17/08/2015.
//  Copyright © 2015 Parse. All rights reserved.
//

import UIKit
import Parse

class SwipingViewController: UIViewController {

    @IBOutlet weak var userImage: UIImageView!
    
    var displayedUserId = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let gesture = UIPanGestureRecognizer(target: self, action: Selector("wasDragged:"))
        userImage.addGestureRecognizer(gesture)
        
        userImage.userInteractionEnabled = true
        
        
        PFGeoPoint.geoPointForCurrentLocationInBackground {
            
            (geoPoint: PFGeoPoint?, error: NSError?) -> Void in
            
            if let geoPoint = geoPoint {
                PFUser.currentUser()?["location"] = geoPoint
                PFUser.currentUser()?.save()
            }
        }
        
        updateImage()
    }
    
    func updateImage() {
        let query = PFUser.query()
        
        var interestedIn = "male"
        
        if (PFUser.currentUser()?["interestedInWomen"])! as! Bool == true {
            interestedIn = "female"
        }
        var isFemale = true
        
        if PFUser.currentUser()!["gender"]! as! String == "male" {
            isFemale = false
        }
        
        query!.whereKey("gender", equalTo: interestedIn)
        query!.whereKey("interestedInWomen", equalTo: isFemale)
        
        var ignoredUsers = [""]
        if let acceptedUsers = PFUser.currentUser()?["accepted"] {
            ignoredUsers += acceptedUsers as! Array
        }
        
        if let rejectedUsers = PFUser.currentUser()?["rejected"] {
            ignoredUsers += rejectedUsers as! Array
        }
        
        query?.whereKey("objectId", notContainedIn: ignoredUsers)
        query!.limit = 1
        
        query!.findObjectsInBackgroundWithBlock {
            (object: [AnyObject]?, error: NSError?) -> Void in
            
            if error != nil {
                print(error)
            } else if let objects = object as? [PFObject] {
                for object in objects {
                    
                    self.displayedUserId = object.objectId!
                    let imagefile = object["image"] as! PFFile
                    
                    imagefile.getDataInBackgroundWithBlock {
                        (imageData: NSData?, error: NSError?) -> Void in
                        
                        if error != nil {
                            print(error)
                        } else {
                            if let data = imageData {
                                self.userImage.image = UIImage(data: data)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func wasDragged(gesture: UIPanGestureRecognizer) {
        let translation = gesture.translationInView(self.view)
        let label = gesture.view!
        
        let width = self.view.bounds.width/2
        let height = self.view.bounds.height/2
        label.center = CGPoint(x: width + translation.x, y: height + translation.y)
        
        let xFromCenter = label.center.x - width
        
        let scale = min(100 / abs(xFromCenter), 1)
        
        var rotation = CGAffineTransformMakeRotation(xFromCenter / 200)
        
        var stretch = CGAffineTransformScale(rotation, scale, scale)
        
        label.transform = stretch
        
        
        if gesture.state == UIGestureRecognizerState.Ended {
            
            var acceptedOrRejected = ""
            if label.center.x < 100 {
                print("Not Chosen " + displayedUserId)
                
                acceptedOrRejected = "rejected"
            } else if label.center.x > self.view.bounds.width - 100 {
                print ("Chosen " + displayedUserId)
                
                acceptedOrRejected = "accepted"
            }
            
            if acceptedOrRejected != ""{
            
                PFUser.currentUser()?.addUniqueObjectsFromArray([displayedUserId], forKey: acceptedOrRejected)
                PFUser.currentUser()?.save()
            }
            rotation = CGAffineTransformMakeRotation(0)
            
            stretch = CGAffineTransformScale(rotation, 1, 1)
            
            label.transform = stretch
            label.center = CGPoint(x: width, y: height)
            
            updateImage()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "logOut" {
            PFUser.logOut()
        }
    }

}
